const { resolve } = require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const NormalModuleReplacementPlugin = require('webpack').NormalModuleReplacementPlugin;
// const FaviconsWebpackPlugin = require("favicons-webpack-plugin");

const env = process.env.NODE_ENV || 'local';
const faviconPath = 'assets/favicons/favicon.ico';

module.exports = {
  resolve: {
    extensions: ['.mjs', '.ts', '.tsx', '.js', '.jsx'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
    plugins: [new TsconfigPathsPlugin()],
  },
  context: resolve(__dirname, '../../src'),
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader', 'source-map-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.tsx?$/,
        use: ['babel-loader', 'awesome-typescript-loader'],
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loaders: ['file-loader?hash=sha512&digest=hex&name=img/[hash].[ext]'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  plugins: [
    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      template: 'index.html',
      favicon: faviconPath,
      filename: 'index.html',
    }),
    new NormalModuleReplacementPlugin(/(.*)-ENV(\.*)/, (resource) => {
      resource.request = resource.request.replace(/-ENV/, `-${env}`);
    }),
    // new FaviconsWebpackPlugin("./assets/favicons/favicon.png")
  ],
  performance: {
    hints: false,
  },
};
