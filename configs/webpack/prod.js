const merge = require('webpack-merge');
const { resolve } = require('path');
const webpack = require('webpack');
const commonConfig = require('./common');
const env = process.env.NODE_ENV;

const publicUrl = '';

module.exports = merge(commonConfig, {
  mode: 'production',
  entry: {
    main: ['babel-polyfill', './index.tsx'],
  },
  output: {
    filename: (chunkData) => (chunkData.chunk.name === 'main' ? 'js/bundle.[hash].min.js' : 'js/[name].js'),
    path: resolve(__dirname, '../../dist'),
    publicPath: publicUrl,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env || 'production'),
    }),
  ],
});
