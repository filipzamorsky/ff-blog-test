import { Main } from './modules/pages/Main';
import { Error } from './modules/layout/components/Error';

export const routes = [
  {
    component: Main,
    exact: true,
    path: '/',
  },
  {
    component: Error,
    exact: true,
    path: '*',
  },
];
