import * as React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from 'emotion-theming';
import { App } from './App';
import { theme } from './appTheme';

const rootElementId = 'root';

const rootEl: HTMLElement = document.getElementById(rootElementId);

render(
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </BrowserRouter>,
  rootEl,
);
