import * as React from 'react';
import { useState, useEffect } from 'react';
import Select from 'react-select';
import axios from 'axios';
import { API_URL, options } from '../utils';
import { UserState } from '../interfaces';
import { theme } from '~/appTheme';
import { Texts } from '~/texts';
import { Loading } from '../layout/ui/Loading';
import { Error } from '../layout/ui/Error';
import { FullWidthWrapper } from '../layout/wrappers/FullWidthWrapper';
import { ContentWrapper } from '../layout/wrappers/ContentWrapper';
import { UserImage } from '../layout/components/UserImage';
import { UserDetail } from '../layout/components/UserDetail';
import { SelectWrapper } from '../layout/wrappers/SelectWrapper';
import { Posts } from '../layout/components/Posts';
import { H1 } from '../layout/typography/H1';
import { H2 } from '../layout/typography/H2';

const initialState = {
  user: {
    id: 1,
    name: Texts.EMPTY_STRING,
    username: Texts.USER,
    address: {
      street: Texts.EMPTY_STRING,
      city: Texts.EMPTY_STRING,
      zipcode: Texts.EMPTY_STRING,
    },
    phone: Texts.EMPTY_STRING,
    email: Texts.EMPTY_STRING,
    website: Texts.EMPTY_STRING,
    company: {
      bs: Texts.EMPTY_STRING,
      catchPhrase: Texts.EMPTY_STRING,
      name: Texts.EMPTY_STRING,
    },
  },
  query: Texts.USER_QUERY,
  isLoading: false,
  isError: false,
};

export const Main: React.FunctionComponent = () => {
  const [isUserState, setIsUserState] = useState<UserState>(initialState);
  const { isLoading, isError, user, query } = isUserState;

  useEffect(() => {
    let isSubscribed = true;
    setIsUserState({ ...isUserState, isLoading: true });
    fetchUserData().then((user) => {
      if (isSubscribed) {
        setIsUserState({ ...isUserState, isLoading: false, user: user });
      }
    });
    return () => (isSubscribed = false);
  }, [query]);

  const fetchUserData = async () => {
    try {
      const response = await axios.get(`${API_URL}/${query}`);
      return response.data;
    } catch (error) {
      setIsUserState({ ...isUserState, isError: true, isLoading: false });
    }
  };

  const handleSelect = (options) => {
    setIsUserState({ ...isUserState, user: { ...user, username: Texts.USER }, query: options.value });
  };

  if (isError) {
    return <Error />;
  }

  return (
    <>
      <FullWidthWrapper background={theme.color.creamy}>
        <ContentWrapper>
          {!isLoading ? (
            <UserDetail user={user} />
          ) : (
            <>
              <UserImage title={Texts.USER} alt={Texts.USER} />
              <H1>{Texts.USER}</H1>
              <Loading />
            </>
          )}
        </ContentWrapper>
      </FullWidthWrapper>

      <FullWidthWrapper background={theme.color.background}>
        <SelectWrapper>
          <Select placeholder={Texts.SELECT_PLACEHOLDER} options={options} onChange={handleSelect} />
        </SelectWrapper>
      </FullWidthWrapper>

      <ContentWrapper paddingBottom="100px">
        <H2>
          {Texts.POSTS_TITLE} {user.username}
        </H2>
        {!isLoading ? <Posts userId={user.id} /> : <Loading />}
      </ContentWrapper>
    </>
  );
};
