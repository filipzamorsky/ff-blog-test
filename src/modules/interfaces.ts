export type UserState = {
  user: User;
  query: string;
  isLoading: boolean;
  isError: boolean;
};

export type PostsState = {
  posts: Post[];
  isLoading: boolean;
  isError: boolean;
};

export type CommentsState = {
  comments: Comment[];
  isLoading: boolean;
  isError: boolean;
  display: string;
};

export type User = {
  id: number;
  name: string;
  username: string;
  address: Address;
  phone: string;
  email: string;
  website: string;
  company: Company;
};

export type Post = {
  userId: number;
  id: number;
  title: string;
  body: string;
};

export type Comment = {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
};

type Address = {
  street: string;
  city: string;
  zipcode: string;
};

type Company = {
  bs: string;
  catchPhrase: string;
  name: string;
};
