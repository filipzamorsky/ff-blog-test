import * as React from 'react';
import { ButtonWrapper } from '../wrappers/ButtonWrapper';

type ButtonText = {
  text: string;
  onClick: () => void;
};

export const Button: React.FunctionComponent<ButtonText> = (props) => {
  const { text, onClick } = props;

  return <ButtonWrapper onClick={onClick}>{text}</ButtonWrapper>;
};
