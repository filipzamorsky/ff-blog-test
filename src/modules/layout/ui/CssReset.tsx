import * as React from 'react';
import { Global, css } from '@emotion/core';
import { theme } from '~/appTheme';

export const CssReset: React.FunctionComponent = () => {
  return (
    <Global
      styles={css`
        html,
        body {
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
          background: ${theme.color.white};
        }
      `}
    />
  );
};
