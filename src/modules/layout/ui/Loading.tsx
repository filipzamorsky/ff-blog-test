import * as React from 'react';
import { SpinnerWrapper } from '../wrappers/SpinnerWrapper';
import Spinner from '~/assets/svg-icons/spinner.svg';

export const Loading: React.FunctionComponent = () => {
  return (
    <>
      <SpinnerWrapper>
        <Spinner />
      </SpinnerWrapper>
    </>
  );
};
