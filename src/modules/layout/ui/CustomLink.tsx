import * as React from 'react';

type Props = {
  href: string;
  text: string;
  target?: string;
};

export const CustomLink: React.FunctionComponent<Props> = (props) => {
  const { href, text, target } = props;

  return (
    <a href={href} title={text} target={target}>
      {text}
    </a>
  );
};
