import * as React from 'react';
import { H1 } from '../typography/H1';
import { Texts } from '~/texts';

export const Error: React.FunctionComponent = () => {
  return <H1>{Texts.ERROR}</H1>;
};
