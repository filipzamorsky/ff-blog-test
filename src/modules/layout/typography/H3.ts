import styled from '@emotion/styled';
import { theme, mediaSize } from '../../../appTheme';

export const H3 = styled.h3`
  font-size: ${theme.fontSize.medium};
  color: ${theme.color.black};
  align-self: flex-start;
  margin: 0;
  padding: 0;

  @media (max-width: ${mediaSize.tablet}) {
    font-size: ${theme.fontSize.normal};
  }
`;
