import styled from '@emotion/styled';
import { theme, mediaSize } from '../../../appTheme';

export const H1 = styled.h1`
  font-size: ${theme.fontSize.title};
  color: ${theme.color.black};
  margin: 30px auto 0 auto;
  padding: 0;

  @media (max-width: ${mediaSize.tablet}) {
    font-size: ${theme.fontSize.subTitle};
  }

  @media (max-width: ${mediaSize.phone}) {
    font-size: ${theme.fontSize.largest};
  }
`;
