import styled from '@emotion/styled';
import { theme, mediaSize } from '../../../appTheme';

export const H2 = styled.h2`
  font-size: ${theme.fontSize.subTitle};
  color: ${theme.color.title};
  margin: 0 0 10px 0;
  padding: 0;

  @media (max-width: ${mediaSize.tablet}) {
    font-size: ${theme.fontSize.largest};
  }

  @media (max-width: ${mediaSize.phone}) {
    font-size: ${theme.fontSize.large};
  }
`;
