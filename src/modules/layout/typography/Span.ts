import styled from '@emotion/styled';
import { theme, mediaSize } from '~/appTheme';

export const Span = styled.span<{ display?: string }>`
  display: block;
  font-size: ${theme.fontSize.medium};
  color: ${theme.color.black};
  line-height: ${theme.lineHeight.heigh};
  margin: 0;
  padding: 0;

  @media (max-width: ${mediaSize.tablet}) {
    font-size: ${theme.fontSize.normal};
  }

  :first-letter {
    text-transform: capitalize;
  }

  a {
    color: ${theme.color.title};
    text-decoration: none;
    text-transform: lowercase;
    cursor: pointer;
  }

  a:hover {
    text-decoration: underline;
  }
`;

export const UserInfoSpan = styled(Span)`
  font-size: ${theme.fontSize.normal};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin: 0;
`;

export const Bold = styled(Span)`
  font-size: ${theme.fontSize.normal};
  color: ${theme.color.black};
  font-weight: ${theme.fontWeight.bold};
`;

export const PostTitleSpan = styled(Span)`
  font-size: ${theme.fontSize.medium};
  font-weight: ${theme.fontWeight.bold};
  text-transform: uppercase;
  margin-top: 10px;
  cursor: pointer;

  @media (max-width: ${mediaSize.tablet}) {
    font-size: ${theme.fontSize.normal};
  }
`;

export const PostContentSpan = styled(Span)`
  color: ${theme.color.subtitle};
  display: ${(props) => props.display};
  height: auto;
  margin-top: 10px;
  margin-left: 25x;
`;

export const CommentsSpan = styled(Span)`
  height: auto;
  margin-top: 10px;
  margin-left: 25px;
`;

export const CommentSpan = styled(Span)`
  color: ${theme.color.subtitle};
  margin-top: 10px;
`;

export const ToogleSpan = styled(Span)`
  width: 75px;
  font-size: ${theme.fontSize.medium};
  font-weight: ${theme.fontWeight.bold};
  margin: 15px 0;
  cursor: pointer;
`;
