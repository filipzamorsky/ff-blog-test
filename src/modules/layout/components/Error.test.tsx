import * as React from 'react';
import 'jest';
import { cleanup, render } from '@testing-library/react';
import { Error } from './Error';

afterEach(cleanup);

describe('Error page test', () => {
  it('should display Page not found... text', async () => {
    const { container } = render(<Error />, {});
    expect(container.textContent).toContain('Page not found...');
  });
});
