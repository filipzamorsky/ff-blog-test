import * as React from 'react';
import { UserImageWrapper } from '../wrappers/UserImageWrapper';
import userImage from '~/assets/userImage.png';

type Props = {
  title: string;
  alt: string;
};

export const UserImage: React.FunctionComponent<Props> = (props) => {
  const { title, alt } = props;
  return (
    <>
      <UserImageWrapper>
        <img title={title} alt={alt} src={userImage} />
      </UserImageWrapper>
    </>
  );
};

// Image downloaded from https://icons8.com
