import * as React from 'react';
import { User } from '~/modules/interfaces';
import { UserInfoSpan, Bold } from '../typography/Span';
import { CustomLink } from '../ui/CustomLink';
import { Texts } from '~/texts';

type Props = {
  user: User;
};

export const UserInfo: React.FunctionComponent<Props> = (props) => {
  const { email, phone, website, address } = props.user;
  const { city, street, zipcode } = address;

  return (
    <>
      <UserInfoSpan>
        <Bold>Street:</Bold> {street}
      </UserInfoSpan>
      <UserInfoSpan>
        <Bold>City:</Bold> {city}
      </UserInfoSpan>
      <UserInfoSpan>
        <Bold>Zipcode:</Bold> {zipcode}
      </UserInfoSpan>
      <UserInfoSpan>
        <Bold>Phone:</Bold> {phone}
      </UserInfoSpan>
      <UserInfoSpan>
        <Bold>E-mail:</Bold>
        <CustomLink href={`${Texts.MAILTO}${email}`} text={email} />
      </UserInfoSpan>
      <UserInfoSpan>
        <Bold>Website:</Bold>
        <CustomLink href={`${Texts.HTTPS}${website}`} text={website} target={Texts.BLANK} />
      </UserInfoSpan>
    </>
  );
};
