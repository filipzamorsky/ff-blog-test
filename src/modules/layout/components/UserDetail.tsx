import * as React from 'react';
import { User } from '../../interfaces';
import { UserImage } from './UserImage';
import { UserInfo } from './UserInfo';
import { H1 } from '../typography/H1';

type Props = {
  user: User;
};

export const UserDetail: React.FunctionComponent<Props> = (props) => {
  const {
    user: { username: username },
  } = props;

  return (
    <>
      <UserImage title={username} alt={username} />
      <H1>{username}</H1>
      <UserInfo user={props.user} />
    </>
  );
};
