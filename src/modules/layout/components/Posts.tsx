import * as React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { API_URL } from '~/modules/utils';
import { PostsState } from '../../interfaces';
import { Loading } from '../ui/Loading';
import { Error } from '../ui/Error';
import { PostAndComments } from './PostAndComments';
import { Span } from '../typography/Span';
import { Texts } from '~/texts';

type Props = {
  userId: number;
};

const initialState = {
  posts: [
    {
      userId: 0,
      id: 0,
      title: Texts.EMPTY_STRING,
      body: Texts.EMPTY_STRING,
    },
  ],
  isLoading: false,
  isError: false,
};

export const Posts: React.FunctionComponent<Props> = (props) => {
  const [isPostsState, setIsPostsState] = useState<PostsState>(initialState);
  const { isLoading, isError, posts } = isPostsState;
  const { userId } = props;

  useEffect(() => {
    let isSubscribed = true;
    setIsPostsState({ ...isPostsState, isLoading: true });

    fetchPostsData().then((posts) => {
      if (isSubscribed) {
        setIsPostsState({ ...isPostsState, isLoading: false, posts: posts });
      }
    });
    return () => (isSubscribed = false);
  }, []);

  const fetchPostsData = async () => {
    try {
      const response = await axios.get(`${API_URL}${Texts.POSTS_QUERY}${userId}`);
      return response.data;
    } catch (error) {
      setIsPostsState({ ...isPostsState, isError: true, isLoading: false });
    }
  };

  if (isLoading) {
    return <Loading />;
  }

  if (isError) {
    return <Error />;
  }

  return (
    <>{posts?.length > 0 ? posts.map((post) => <PostAndComments key={post.id} postId={post.id} post={post} />) : <Span>{Texts.NO_POSTS}</Span>}</>
  );
};
