import * as React from 'react';
import axios from 'axios';
import { useState } from 'react';
import { CommentsState, Post } from '../../interfaces';
import { API_URL } from '~/modules/utils';
import { Loading } from '../ui/Loading';
import { Error } from '../ui/Error';
import { PostTitleSpan, PostContentSpan, CommentsSpan, CommentSpan, ToogleSpan, Bold } from '../typography/Span';
import { CommentWrapper } from '../wrappers/CommentWrapper';
import { H3 } from '../typography/H3';
import { Texts } from '~/texts';

type Props = {
  postId: number;
  post: Post;
};

const initialState = {
  comments: [
    {
      postId: 0,
      id: 0,
      name: Texts.EMPTY_STRING,
      email: Texts.EMPTY_STRING,
      body: Texts.EMPTY_STRING,
    },
  ],
  isLoading: false,
  isError: false,
  display: Texts.NONE,
};

export const PostAndComments: React.FunctionComponent<Props> = (props) => {
  const [isCommentsState, setIsCommentsState] = useState<CommentsState>(initialState);
  const { isLoading, isError, comments, display } = isCommentsState;
  const { postId, post } = props;

  const fetchCommentsData = async () => {
    try {
      const response = await axios.get(`${API_URL}${Texts.COMMENTS_QUERY}${postId}`);
      return response.data;
    } catch (error) {
      setIsCommentsState({ ...isCommentsState, isError: true, isLoading: false });
    }
  };

  const handleClick = () => {
    let isSubscribed = true;
    setIsCommentsState({ ...isCommentsState, isLoading: true, display: Texts.BLOCK });
    fetchCommentsData().then((comments) => {
      if (isSubscribed) {
        setIsCommentsState({ ...isCommentsState, isLoading: false, comments: comments, display: Texts.BLOCK });
      }
    });
    return () => (isSubscribed = false);
  };

  const handleOnToogleClick = () => {
    setIsCommentsState({ ...isCommentsState, display: Texts.NONE });
  };

  if (isError) {
    return <Error />;
  }

  return (
    <>
      <PostTitleSpan onClick={handleClick}>{post.title}</PostTitleSpan>

      <PostContentSpan display={display}>
        {post.body}

        <CommentsSpan>
          <H3>{Texts.COMMENTS}</H3>

          {comments?.length > 1 && !isLoading ? (
            comments.map((comment) => {
              const { id, body, name, email } = comment;
              return (
                <CommentWrapper key={id}>
                  <CommentSpan>{body}</CommentSpan>
                  <Bold>{`${Texts.COMMENTED_BY} ${name} - ${email}`}</Bold>
                </CommentWrapper>
              );
            })
          ) : (
            <Loading />
          )}

          <ToogleSpan onClick={handleOnToogleClick}>{Texts.TOOGLE}</ToogleSpan>
        </CommentsSpan>
      </PostContentSpan>
    </>
  );
};
