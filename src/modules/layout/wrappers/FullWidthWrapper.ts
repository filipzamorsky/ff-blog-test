import styled from '@emotion/styled';

export const FullWidthWrapper = styled.div<{ background?: string }>`
  width: 100%;
  height: auto;
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;
  overflow: visible;
  background: ${(props) => props.background};
`;
