import styled from '@emotion/styled';

export const UserImageWrapper = styled.div`
  height: auto;
  margin: 30px auto 0 auto;
  padding: 0;
`;
