import styled from '@emotion/styled';
import { theme } from '~/appTheme';

export const CommentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  padding-bottom: 3px;
  border-bottom: 1px solid ${theme.color.subtitle};
`;
