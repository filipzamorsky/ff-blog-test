import styled from '@emotion/styled';
import { mediaSize } from '~/appTheme';

export const SelectWrapper = styled.div`
  width: 200px;
  height: auto;
  margin: 30px auto;
  padding: 0;
  position: relative;
  z-index: 99;
  overflow: visible;

  @media (max-width: ${mediaSize.phone}) {
    width: calc(100% - 40px);
  }
`;
