import styled from '@emotion/styled';
import { theme, mediaSize } from '~/appTheme';

export const ContentWrapper = styled.div<{ paddingBottom?: string }>`
  width: 700px;
  min-height: 345px;
  height: auto;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  padding: 0;
  padding-bottom: ${(props) => props.paddingBottom || 0};
  overflow: hidden;
  font-family: ${theme.fontFamily.main};
  font-style: normal;
  background: ${theme.color.transparent};
  -webkit-touch-callout: none; /* iOS Safari */
  -webkit-user-select: none; /* Safari */
  -khtml-user-select: none; /* Konqueror HTML */
  -moz-user-select: none; /* Old versions of Firefox */
  -ms-user-select: none; /* Internet Explorer/Edge */
  user-select: none; /* Non-prefixed version, currently supported by Chrome, Opera and Firefox */

  @media (max-width: ${mediaSize.tablet}) {
    width: calc(100% - 40px);
  }
`;
