import styled from '@emotion/styled';
import { theme } from '~/appTheme';

export const ButtonWrapper = styled.div`
  width: 200px;
  height: 50px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  color: ${theme.color.white};
  background: ${theme.color.red};
  border-radius: 9px;
  padding: 0 0 3px 0;
  margin: 5px auto;

  :hover {
    color: ${theme.color.black};
    cursor: pointer;
  }
  :active {
    opacity: 0.9;
  }
`;
