export const API_URL = 'https://jsonplaceholder.typicode.com';

export const options = [
  { value: 'users/1', label: 'Bret' },
  { value: 'users/2', label: 'Antonette' },
  { value: 'users/3', label: 'Samantha' },
  { value: 'users/4', label: 'Karianne' },
];
