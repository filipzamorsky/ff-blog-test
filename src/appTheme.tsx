export const theme = {
  color: {
    black: 'black',
    blue: 'blue',
    gray: 'gray',
    green: 'green',
    red: 'red',
    white: 'white',
    yellow: 'yellow',
    bordeaux: '#5f0000',
    creamy: '#fdb580',
    transparent: 'transparent',
    background: '#fff',
    title: '#424242',
    subtitle: '#353535',
  },
  fontFamily: { main: 'Lato' },
  fontSize: {
    title: '36px',
    // tslint:disable-next-line:object-literal-sort-keys
    subTitle: '27px',
    largest: '24px',
    large: '20px',
    medium: '16px',
    normal: '14px',
    small: '10px',
  },
  fontWeight: {
    title: 700,
    // tslint:disable-next-line:object-literal-sort-keys
    subTitle: 400,
    bold: 700,
    normal: 400,
    light: 300,
    thin: 100,
  },
  lineHeight: {
    title: '23px',
    // tslint:disable-next-line:object-literal-sort-keys
    subTitle: '21px',
    heigh: '18px',
    normal: '16px',
    low: '12px',
  },
};

export const mediaSize = {
  tabletWidth: '1085px',
  tablet: '992px',
  phone: '605px',
};
