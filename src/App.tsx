import * as React from 'react';
import { Router } from './Router';
import { CssReset } from './modules/layout/ui/CssReset';

export const App: React.FunctionComponent = () => {
  return (
    <>
      <CssReset />
      <Router />
    </>
  );
};
