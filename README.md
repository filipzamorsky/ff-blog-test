# README

- this is a simple user/posts/comments app
- version 0.0.1

### Set-up

- clone repository
- install node modules `$yarn install`

### Start app

- run `$yarn start`
- open browser at `https://localhost:8080/`
- you will find other commands within the `./package.json` file

### Run tests

- run `$yarn test` (not too many tests written yet, but you can try)

### Dependencies

- node
- yarn
- typescript
- axios
- express
- react
- react-router
- emotion styled components

### Contribution guidelines

- writing tests (under development)
- deployment instructions (under development)
- code review (under development)
- other guidelines (under development)

### Repo owner

- filipzamorsky(at)gmail(dot)com
