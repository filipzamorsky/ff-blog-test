const express = require("express");
const path = require("path");
const app = express();
const portNumber = 3000;

app.get("/", (req, res) => {
  res.sendFile(path.resolve(__dirname, "dist", "index.html"));
});

app.use(express.static(path.resolve(__dirname, "dist")));

app.listen(portNumber, () =>
  console.log("App is listening on port", portNumber),
);
